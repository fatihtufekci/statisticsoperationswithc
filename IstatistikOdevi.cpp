#include <graphics.h>
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <math.h>
#include <dos.h>

int sinif_sayisi = 5;


void frekansBul( int y[], int z[], int k[], int sube_sayisi);
void artanFrekans(int x[], int y[]);
void tabloOlustur(int x[], int y[], float z[], int k[], int sube_sayisi);
int modBul( int x[], int y , int sube_sayisi);
void medyanBul(int m[], int sube_sayisi);
float ortBul( int y[], float mi[], int sube_sayisi);
float standartSapma(int frekans[], float mi[], int sube_sayisi);

main( ){

    int sube_sayisi;

	char istatistik_sorusu[] = "Bir AVM nin magazalarina gun icerisinde gelen musteri sayisine gore bazi hesaplamalar yapilacaktir\n";
	printf("%s", istatistik_sorusu);
    int i;

    printf("AVM'deki magaza sayisini giriniz..: ");
    scanf("%d", &sube_sayisi);

    printf("\nHerbir magazaya gun icerisinde gelen musteri sayilarini giriniz..: ");
    int musteri_sayilari[sube_sayisi];
    for(i=0; i<sube_sayisi ;i++){
        scanf("%d", &musteri_sayilari[i]);
    }
    printf("\n");
	for(i=0; i<sube_sayisi ; i++){
		printf("%d  ",  musteri_sayilari[i] );
	}
	char a_sorusu[] = "\n\na-)Buna gore tum sinif sayisi 5 olan verilerin sinif araliklarini ve sinif frekansini olusturunuz..";
	char b_sorusu[] = "b-)Artan ve oransal frekansi hesaplayiniz..";
	char c_sorusu[] = "c-)Ortalama, mod ve medyani hesaplayiniz..";
	char d_sorusu[] = "d-)Standart sapma ve degisim katsayisini hesaplayiniz..";
	char e_sorusu[] = "e-)Basiklik ve carpiklik katsayisini bulunuz..";
	char f_sorusu[] = "f-)Grafigini ciziniz..\n";
	printf("%s\n%s\n%s\n%s\n%s\n%s\n", a_sorusu, b_sorusu, c_sorusu, d_sorusu, e_sorusu, f_sorusu );

	printf("\nLutfen cozumleri gormek icin C tusuna basip ENTER'a tiklayiniz..: ");
	char tus;
	scanf("%s", &tus);

	printf("\n\na ve b-) ");
	printf("\n\n");

	//artan frekans
	int artan[sinif_sayisi];
	//artanFrekans(artan, frekans);

	// Dizideki maximum degeri buluyoruz..
	int max;
	max=musteri_sayilari[0];
	for(i=1; i<sube_sayisi ; i++){
		if( musteri_sayilari[i] > max ){
			max = musteri_sayilari[i];
		}
	}
	int sinif_sayi = (max/sinif_sayisi) + 1 ;
	printf("maximum deger: %d\n", max);
	printf("\nsinif araligi: %d/5 ~= %d\n\n", max, sinif_sayi);

	// S�n�f araliklarini bulalim
	int sinif_sinir[sinif_sayisi];
	int toplam = 0 ;
	for(i=0; i<7; i++){
		sinif_sinir[i] = toplam;
		toplam = toplam + sinif_sayi;
	}

	int frekans[]={ 0, 0, 0, 0, 0, 0 };

	//Sinif frekanslari olusturuyoruz..
	frekansBul( musteri_sayilari, frekans, sinif_sinir, sube_sayisi);


	//mi'yi buluyoruz..
	float mi[sinif_sayisi];
	for(i=1; i<sinif_sayisi+1 ; i++){
		mi[i-1] = ( sinif_sinir[i] + sinif_sinir[i-1] ) / 2.0 ;
	}

	// Synyf aralygynyn orta degerleri..
	tabloOlustur( sinif_sinir, frekans, mi, artan, sube_sayisi);


	printf("\n\nc-) ");

	// Ortalamayi buluyoruz..
	printf("\n\n");
	float ort = ortBul(frekans, mi, sube_sayisi);
	printf(" Ortalama: %.2f\n\n", ort);


	// Mod bulma
	int mod_degeri ;
	printf(" Mod = %d\n\n", modBul( musteri_sayilari, mod_degeri , sube_sayisi));



	//Medyan bulma
	medyanBul( musteri_sayilari, sube_sayisi);


	printf("\nd-)\n\n");

	//Standart sapma..
	float standart_sapma;
	standart_sapma = standartSapma(frekans, mi, sube_sayisi);
	printf(" Standart sapma..: %.2f\n\n", standart_sapma);



	//De?i?im katsayysy..
	printf(" Degisim katsayisi = %.2f\n\n", (standart_sapma/ort)*100);


	printf("\ne-)\n\n");

	// Carpiklik katsayysy..
	float moment3;
	float moment3_islem;
	float moment3_toplam=0.0;
	float carpiklik_katsayisi;
	float carpiklik_degeri;
	ort = ortBul(frekans, mi, sube_sayisi);
	standart_sapma = standartSapma(frekans, mi, sube_sayisi);

	for( i=1 ; i<sinif_sayisi+1 ; i++ ){
		moment3_islem = (frekans[i])*pow((mi[i-1]-ort),3) ;
		moment3_toplam += moment3_islem;
	}
	moment3 = (moment3_toplam / sube_sayisi)+0.0;

	carpiklik_degeri = moment3 / ( pow(standart_sapma,3) );
	carpiklik_katsayisi = carpiklik_degeri;

	if( carpiklik_katsayisi > 0 ){
		printf(" Carpiklik katsayisi: %.2f ve > 0 oldugundan seri saga carpik\n\n", carpiklik_katsayisi);
	}
	else if( carpiklik_katsayisi < 0 ){
		printf(" Carpiklik katsayisi: %.2f ve < 0 oldugundan seri sola carpik\n\n", carpiklik_katsayisi);
	}
	else{
		printf(" Carpiklik katsayisi: %.2f ve = 0 oldugundan seri simetrik\n\n", carpiklik_katsayisi);
	}



	// Basyklyk katsayysy..
	float moment4;
	float moment4_islem;
	float moment4_toplam=0.0;
	float basiklik_katsayisi;
	float basiklik_degeri;
	ort = ortBul(frekans, mi, sube_sayisi);
	standart_sapma = standartSapma(frekans, mi, sube_sayisi);

	for( i=1 ; i<sinif_sayisi+1 ; i++ ){
		moment4_islem = (frekans[i])*pow((mi[i-1]-ort),4) ;
		moment4_toplam += moment4_islem;
	}
	moment4 = (moment4_toplam / sube_sayisi)+0.0;

	basiklik_degeri = moment4 / ( pow(standart_sapma,4) );
	basiklik_katsayisi = basiklik_degeri;

	if( basiklik_katsayisi > 3 ){
		printf(" Basiklik katsayisi: %.2f ve >3 oldugundan seri sivri veya yuksek\n\n", basiklik_katsayisi);
	}
	else if( basiklik_katsayisi < 3 ){
		printf(" Basiklik katsayisi: %.2f ve < 3 oldugundan seri basik\n\n", basiklik_katsayisi);
	}
	else{
		printf(" Basiklik katsayisi: %.2f ve = 3 oldugundan seri normal\n\n", basiklik_katsayisi);
	}


	printf("\nf-) GRAFIK CIZILIYOR... \n\n");




	// Grafik �izimi..

	int surucu = DETECT, grmode;
	int errorcode=0;
	initgraph(&surucu, &grmode, "C:\\TC\\BGI");

	errorcode = graphresult();
	if (errorcode != grOk)
	{
		printf("Grafik hatasi: (#%d kodlu hata)  %s\n", errorcode, grapherrormsg(errorcode));
		printf("Programi durdurmak icin bir tusa basiniz");
		getch();
		exit(1);
	}


	setcolor(WHITE);

	line(100,420,100,60);
   	line(100,420,600,420);

	setbkcolor(BLACK);

	outtextxy(95,35,"F");
  	outtextxy(610,405,"X");
   	outtextxy(85,415,"O");



    outtextxy(99,425,"x(0)");
    outtextxy(148,425,"x(1)");
    outtextxy(198,425,"x(2)");
    outtextxy(247,425,"x(3)");
    outtextxy(297,425,"x(4)");
    outtextxy(347,425,"x(5)");

    int j;
    int cikartma;
    int buyuk[5]={0, 0, 0, 0, 0};
    int kucuk[5]={0, 0, 0, 0, 0};

    for(j=1; j<6 ; j++){
        if(frekans[1] > frekans[j]){
            cikartma = frekans[1] - frekans[j];
            kucuk[j-1] = cikartma*10;
            cikartma=0;
        }
        else if(frekans[1] < frekans[j]){
            cikartma = frekans[j] - frekans[1];
            buyuk[j-1] = cikartma*10;
            cikartma = 0 ;
        }
        else{
            cikartma = 0;
        }

    }

    int ekle[5];
    for(i=0; i<5 ; i++){
        ekle[i]= kucuk[i] - buyuk[i];
    }

	setfillstyle(INTERLEAVE_FILL,BLUE);
   	bar(110,250+ekle[0],160,419);

    setfillstyle(INTERLEAVE_FILL,RED);
	bar(160,250+ekle[1],210,419);

 	setfillstyle(INTERLEAVE_FILL,GREEN);
	bar(210,250+ekle[2],260,419);

 	setfillstyle(INTERLEAVE_FILL,MAGENTA);
 	bar(260,250+ekle[3],310,419);

 	setfillstyle(INTERLEAVE_FILL,BROWN);
 	bar(310,250+ekle[4],360,419);


	getch( );
	closegraph( );
	return 0;
}



void frekansBul( int musteri_sayilari[], int frekans[], int sinif_sinir[], int sube_sayisi){
	int i;
	for(i=0; i<sube_sayisi ; i++){

		if( musteri_sayilari[i]<=sinif_sinir[1] && musteri_sayilari[i]>=sinif_sinir[0] ){
			frekans[1]++;
		}
		else if( musteri_sayilari[i]<=sinif_sinir[2] && musteri_sayilari[i]>sinif_sinir[1] ){
			frekans[2]++;
		}
		else if( musteri_sayilari[i]<=sinif_sinir[3] && musteri_sayilari[i]>sinif_sinir[2] ){
			frekans[3]++;
		}
		else if( musteri_sayilari[i]<=sinif_sinir[4] && musteri_sayilari[i]>sinif_sinir[3] ){
			frekans[4]++;
		}
		else if( musteri_sayilari[i]<=sinif_sinir[5] && musteri_sayilari[i]>sinif_sinir[4] ){
			frekans[5]++;
		}
		else{
			printf(" ");
		}
	}
}

void artanFrekans(int artan[], int frekans[]){
	float sum = 0 ;
	int i;
	artan[0]=0;
	for(i=1; i<sinif_sayisi+1; i++){
		sum = sum + frekans[i];
		artan[i] = sum ;
	}
}

void tabloOlustur(int sinif_sinir[], int frekans[], float mi[], int artan[], int sube_sayisi){
	artanFrekans(artan, frekans);
	int i;
	printf("   x\t\t f\t\t mi\t\tOransal\t\tArtan\n");
	printf(" -----\t\t-----\t\t------\t\t-------\t\t-----\n");
	for(i=1; i<=sinif_sayisi ; i++){
		printf(" %3d -%3d\t %d\t\t %.2f\t\t%.2f\t\t %d\n", sinif_sinir[i-1] ,sinif_sinir[i], frekans[i], mi[i-1], (frekans[i]/(sube_sayisi+0.0)), artan[i]);
	}
}

int modBul( int musteri_sayilari[], int mod_degeri , int sube_sayisi){
	int sayac;
	int mod = 0 ;

	int k,j;

	for( k=0; k<sube_sayisi ; k++ ){
		sayac = 0;
		for(j=0 ; j<sube_sayisi ; j++ ){
			if(musteri_sayilari[k]==musteri_sayilari[j]){
				sayac++;
			}
			if(sayac>mod){
				mod = sayac;
				mod_degeri = musteri_sayilari[k];
			}
		}
	}
	return mod_degeri;
}

void medyanBul(int musteri_sayilari[], int sube_sayisi){
	int tasiyici=0;
	int medyan;
	int k,j;

	for( k=0 ; k<sube_sayisi ; k++ ){
		for( j=0 ; j<sube_sayisi ; j++ ){
			if( musteri_sayilari[j] > musteri_sayilari[j+1]  ){
				tasiyici = musteri_sayilari[j];
				musteri_sayilari[j] = musteri_sayilari[j+1];
				musteri_sayilari[j+1] = tasiyici;
			}
		}
	}

	if( (int)sube_sayisi % 2 == 0 ){
		medyan = (sube_sayisi/2.0)+0.0;
		printf(" Medyan = %d\n", ( musteri_sayilari[ medyan ] + musteri_sayilari[ medyan+1 ] ) / 2 ) ;
	}
	else{
		medyan=(sube_sayisi/2.0)+0.0;
		printf(" Medyan = %d\n", musteri_sayilari[medyan] );
	}
}

float ortBul( int frekans[], float mi[], int sube_sayisi){
	float *ort;
	float deger;
	int i;
	float toplam = 0.0;
	for(i=1; i<sinif_sayisi+1 ; i++){
		toplam = toplam + ( frekans[i] * mi[i-1] );
	}
	deger = (toplam / sube_sayisi)+0.0;
	ort = &deger;
	return *ort;
}

float standartSapma(int frekans[], float mi[], int sube_sayisi){
	float *standart_sapma;
	float standart_deger;
	float standart_islem;
	float standart_toplam = 0.0 ;
	float ort = ortBul(frekans, mi, sube_sayisi);
	int i;
	for(i=1 ; i<sinif_sayisi+1 ; i++){
		standart_islem = (frekans[i])*pow((mi[i-1]-ort),2);
		standart_toplam += standart_islem;
	}
	standart_toplam = (standart_toplam / sube_sayisi)+0.0;
	standart_deger = sqrt(standart_toplam);
	standart_sapma = &standart_deger;
	return *standart_sapma;
}





